class AddListIdToWorks < ActiveRecord::Migration[6.1]
  def change
    add_reference :works, :list, null: false, foreign_key: true
  end
end
