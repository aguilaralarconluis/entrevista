class WorksController < ApplicationController
	def new
		@list = List.find(params[:list])
		puts "\n\n\n #{@list.id} \n\n\n"
		redirect_to edit_list_path(list)
	end

	def destroy
		work = Work.find(params[:id])
		work.destroy
		redirect_to edit_list_path
	end

end
