Rails.application.routes.draw do
  resources :lists 
  get "list/:id/work_new", to: "lists#new_work", as: 'new_work'
  delete "list/:list_id/work/:work_id", to: "lists#destroy_work", as: "destroy_work"
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
